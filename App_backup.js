import React from 'react'
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createBottomTabNavigator} from 'react-navigation-tabs'
import {createStackNavigator} from 'react-navigation-stack';
import AccountScreen from './src/screens/AccountScreen';
import SignupScreen from './src/screens/SignupScreen';
import SinginScreen from './src/screens/SinginScreen';
import TrackCreateScreen from './src/screens/TrackCreateScreen';
import TrackDetailScreen from './src/screens/TrackDetailScreen';
import TrackListScreen from './src/screens/TrackListScreen';
import {Provider as AuthProvider} from './src/context/authContext';
import {Provider as LocationProvider} from './src/context/LocationContext';
import {setNavigator} from './src/navigationRef';
import AuthResolveScreen from './src/screens/AuthResolve';

const switchNav = createSwitchNavigator({ //in-order na tanan basahon sa nav, so the first one is authentication
   AuthResolve: AuthResolveScreen,
   loginFlow: createStackNavigator({ 
     Signup: SignupScreen,
     Signin: SinginScreen
   }),
   mainFlow: createBottomTabNavigator({
    trackListFlow: createStackNavigator({
      TrackList: TrackListScreen,
      TrackDetail: TrackDetailScreen
    }),
    TrackCreate: TrackCreateScreen,
    Account: AccountScreen
   })
  });

const App = createAppContainer(switchNav);

export default () =>{
  return(
  <LocationProvider>
    <AuthProvider>
      <App 
        ref={navigator => {
          setNavigator(navigator);
        }} 
      />
    </AuthProvider>
  </LocationProvider>
  );
};