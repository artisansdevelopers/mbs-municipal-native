import React, { useContext } from 'react';
import { Text, TouchableHighlight } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { globalStyles } from '../globalStyles';
import { LinearGradient } from 'expo-linear-gradient';
import { ThemeContext } from '../context/themeContext';

export default function DefaultHeader({ navigation }){

    let theme = useContext(ThemeContext);
    
    return (
        <LinearGradient 
            colors={[theme.secondary_color, theme.primary_color]} 
            start={[0,0]} 
            end={[1,1]}  style={[globalStyles.header, theme.header.border, { alignItems: 'flex-start' }]}>   
            
                <TouchableHighlight  style={globalStyles.header_button} underlayColor='rgba(0,0,0,0.1)' onPress={()=>navigation.goBack()}>
                    <Ionicons name="ios-arrow-back" size={20} style={theme.header.color} />
                </TouchableHighlight>

                <Text style={[globalStyles.header_text,theme.header.color]}>{navigation.getParam('location')}</Text>
        </LinearGradient>
    )
}