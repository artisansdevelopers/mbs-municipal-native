import React, {useState} from 'react';
import {Text, StyleSheet} from 'react-native';
import MapView, {Polyline, Circle} from 'react-native-maps';
import {Context as LocationContext} from '../context/LocationContext';


const Map = () =>{

   return( 
   
   <MapView 
   style={styles.map} 
    initialRegion={{
        latitude: 33.56485,
        longitude: -122.56485,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01  //Deltas is how much zoom
    }}

   >
      {/*  <Circle 
            radius={30}
            strokeColor="rgba(158,158,255,1.0)"
            fillColor="rgba(158,158,255,1.0)"
       /> */}
    </MapView>
   );
};

const styles = StyleSheet.create({
        map : {
            height: 300
        }
})


export default Map;

