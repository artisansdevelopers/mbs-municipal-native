import React, { useContext } from 'react';
import { Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ThemeContext } from '../context/themeContext';
import {withNavigation} from 'react-navigation';
import { globalStyles } from '../globalStyles';

const NavLink = ({navigation, text, routeName, position, style}) =>{

    let theme = useContext(ThemeContext);

    return (
        <TouchableOpacity onPress={() => navigation.navigate(routeName)}>
                <Text style={[{ textAlign: position, color: theme.primary_color }, style]}>{text}</Text>
         </TouchableOpacity>
    )
}

export default withNavigation(NavLink);