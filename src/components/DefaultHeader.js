import React,{ useContext } from 'react';
import { Text } from 'react-native';
import { globalStyles } from '../globalStyles';
import { LinearGradient } from 'expo-linear-gradient';
import { ThemeContext } from '../context/themeContext';

export default function DefaultChildHeader({ navigation }){

    let theme = useContext(ThemeContext);
    
    return (
        <LinearGradient 
            colors={[theme.secondary_color, theme.primary_color]} 
            start={[0,0]} 
            end={[1,1]}  style={[globalStyles.header, theme.header.border, { alignItems: 'center' }]}>            
                <Text style={[globalStyles.header_text, theme.header.color, { paddingLeft: 0  }]}>{navigation.state.routeName}</Text>
        </LinearGradient>
    )
}

