import React from 'react';
import { View, StatusBar, Platform } from 'react-native';

const STATUS_BAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

const StandardStatusBar = ({ backgroundColor, barStyle }) =>(
    <View style={{
            width: "100%",
            height: STATUS_BAR_HEIGHT,
            backgroundColor: backgroundColor
        }}>
        <StatusBar
            barStyle={barStyle}
        />
    </View>
)

export default StandardStatusBar;