import React from 'react';
import { createStackNavigator, TransitionPresets } from 'react-navigation-stack';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { SimpleLineIcons,Entypo,FontAwesome } from 'react-native-vector-icons';
import DefaultHeader from '../components/DefaultHeader';
import DefaultChildHeader from '../components/DefaultChildHeader';
import { themes } from '../context/themeContext'; 

//Screens
import TrackListScreen from '../screens/TrackListScreen';
import TrackDetailScreen from '../screens/TrackDetailScreen';
import TrackCreateScreen from '../screens/TrackCreateScreen';
import AccountScreen from '../screens/AccountScreen';
import LoadingScreen from '../screens/LoadingScreen';

//-------------- Start of Tracks Screens -----------------------------
const TracksScreens = createStackNavigator({
        TrackList: {
            screen: TrackListScreen,
            navigationOptions: ({ navigation }) =>{
                return {
                    header: () => <DefaultHeader navigation={navigation}/>
                }
            }
        },
        TrackDetail: {
            screen: TrackDetailScreen
        },
    },{
    defaultNavigationOptions: ({ navigation }) =>{
        return {
            header: () => <DefaultChildHeader navigation={navigation}/>,
            ...TransitionPresets.SlideFromRightIOS
        }
    }
});

//Remove Bottom Navigator on Child Screens
TracksScreens.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==1){
        return {
            tabBarVisible: false,
        };
    }
    return {
        tabBarVisible: true,
    };
};
//-------------- End of Tracks Screens -----------------------------


//-------------- Start of TrackCreate Screens -----------------------------
const TrackCreateScreens = createStackNavigator({
        TrackCreate: {
            screen: TrackCreateScreen
        }
    },{
    defaultNavigationOptions: ({ navigation }) =>{
        return {
            ...TransitionPresets.SlideFromRightIOS
        }
    }
});

//Remove Bottom Navigator on Child Screens
TrackCreateScreens.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==1){
        return {
            tabBarVisible: false,
        };
    }
    return {
        tabBarVisible: true,
    };
};
//-------------- End of TrackCreate Screens -----------------------------


//-------------- Start of Account Screens -----------------------------
const AccountScreens = createStackNavigator({
        Account: {
            screen: AccountScreen,
            navigationOptions: ({ navigation }) =>{
                return {
                    header: () => <DefaultHeader navigation={navigation}/>
                }
            }
        },
        Loading: {
            screen: LoadingScreen
        }
    },{
    defaultNavigationOptions: ({ navigation }) =>{
        return {
            ...TransitionPresets.SlideFromRightIOS
        }
    }
});

//Remove Bottom Navigator on Child Screens
AccountScreens.navigationOptions = ({ navigation }) => {
    if(navigation.state.index==1){
        return {
            tabBarVisible: false,
        };
    }
    return {
        tabBarVisible: true,
    };
};
//-------------- End of Account Screens -----------------------------

//Apply Bottom Navigator to All
const MainFlow =  createMaterialBottomTabNavigator({
    Tracks: {
        screen: TracksScreens,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
              <SimpleLineIcons name="list" size={20} color={tintColor}/>
            )
        },
    },
    TrackCreate: {
        screen: TrackCreateScreens,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
              <Entypo name="add-to-list" size={20} color={tintColor}/>
            )
        },
    },
    Account: {
        screen: AccountScreens,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
              <FontAwesome name="user-o" size={20} color={tintColor}/>
            )
        },
    }
},
{
    initialRouteName: 'Tracks',
    activeColor: themes.default.primary_color,
    inactiveColor: 'gray',
    barStyle: { 
        backgroundColor: themes.default.bottom_tabs.background,
        elevation: 0,
        borderTopWidth: 1,
        borderTopColor: themes.default.bottom_tabs.border
    },
})

export default MainFlow;


  