import React from 'react';
import { createStackNavigator, TransitionPresets  } from 'react-navigation-stack';

//Screens
import SignInScreen from '../screens/SignInScreen';
import SignUpScreen from '../screens/SignUpScreen';
import ForgotPasswordScreen from '../screens/ForgotPasswordScreen';
import LoadingScreen from '../screens/LoadingScreen';

const screens = {
    SignIn: {
        screen: SignInScreen
    },
    SignUp: {
        screen: SignUpScreen
    },
    ForgotPassword: {
        screen: ForgotPasswordScreen
    },
    Loading: {
        screen: LoadingScreen
    }
}

const LoginFlow = createStackNavigator(screens,{
    defaultNavigationOptions: {
        headerShown: false,
        ...TransitionPresets.ScaleFromCenterAndroid
    },
});

export default LoginFlow;


  