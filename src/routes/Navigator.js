import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import AuthResolve from '../screens/AuthResolve';
import LoginFlow from './LoginFlow';
import MainFlow from './MainFlow';

const switchNav = createSwitchNavigator({ 
    AuthResolve, //in-order na tanan basahon sa nav, so the first one is authentication
    LoginFlow,
    MainFlow
});
 
export default createAppContainer(switchNav);