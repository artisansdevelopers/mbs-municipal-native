import React, {useState, useEffect} from 'react';
import {requestPermissionsAsync, watchPositionAsync, Accuracy} from 'expo-location';

export default () =>{
    const [err, setErr] = useState(null)

    const startWatching = async(callback) => {
        try{
                await requestPermissionsAsync();
                await watchPositionAsync(
                    {
                        accuracy: Accuracy.BestForNavigation,
                        timeInterval: 1000, //1 second
                        distanceInterval: 10 //update every 10 meters
                    }, 
                    callback
                );
        }catch(e){
            setErr(e);
        }
    }

      
    useEffect(() => {
        startWatching();
    }, [])

    return {err}
}