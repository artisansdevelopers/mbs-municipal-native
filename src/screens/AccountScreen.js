import React, { useContext } from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import { Context as AuthContext } from '../context/authContext';
import { ThemeContext } from '../context/themeContext';
import { TouchableRipple } from 'react-native-paper';
import {  Ionicons, FontAwesome } from 'react-native-vector-icons';
import { globalStyles } from '../globalStyles';

const AccountScreen = ({navigation}) =>{

    const { state, signOut } = useContext(AuthContext);
    let theme = useContext(ThemeContext);

    return (
        <View style={[globalStyles.container, theme.container]}>
            <TouchableRipple 
                    onPress={()=> console.log('Info') } 
                    rippleColor="rgba(0, 0, 0, 0.1)">
                    <View style={styles.list}>
                        <View style={styles.profile_container}>
                            <Image source={{ uri:"https://akns-images.eonline.com/eol_images/Entire_Site/2017423/rs_600x600-170523170627-600.Chloe-Not-Impressed-YouTube.ms.052317.jpg?fit=around|600:467&crop=600:467;center,top&output-quality=90" }} style={styles.profile_image} />
                        </View>  
                        <View style={{ marginLeft: 10 }}>
                            <Text>Super Admin</Text>
                            <Text style={{ color: 'gray' }}>admin@admin.com</Text>
                        </View>
                    </View>
            </TouchableRipple>
          
            <View>
                <TouchableRipple 
                    onPress={()=> console.log('Profile') } 
                    rippleColor="rgba(0, 0, 0, 0.1)">
                    <View style={styles.list}>
                        <FontAwesome 
                            name="user-circle-o" 
                            size={15} 
                            style={{ marginRight: 10, color: theme.primary_color }}/><Text>Profile</Text>
                    </View>
                </TouchableRipple>

                <TouchableRipple 
                    onPress={()=> console.log('Settings') } 
                    rippleColor="rgba(0, 0, 0, 0.1)">
                    <View style={styles.list}>
                        <Ionicons 
                            name="ios-settings" 
                            size={20} 
                            style={{ marginRight: 10, color: theme.primary_color }}/><Text>Settings</Text>
                    </View>
                </TouchableRipple>

                <TouchableRipple 
                    onPress={()=>{signOut(), navigation.navigate('Loading', {text: 'Logging out...'}) }} 
                    rippleColor="rgba(0, 0, 0, 0.1)">
                    <View style={styles.list}>
                        <Ionicons 
                            name="md-exit" 
                            size={20} 
                            style={{ marginRight: 10, color: theme.primary_color }}/><Text>Sign Out</Text>
                    </View>
                </TouchableRipple>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    list:{
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcde',
        flexDirection: 'row',
        alignItems: 'center'
    },
    profile_container: {
        height: 60,
        width: 60,
        borderRadius: 50,
        overflow: 'hidden',
    },
    profile_image: {
        height: 60,
        width: 60,
        borderRadius: 50,
    },
});


export default AccountScreen;

