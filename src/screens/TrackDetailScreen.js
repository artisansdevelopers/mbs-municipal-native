import React, { useContext } from 'react';
import { View, ScrollView, StyleSheet, Text } from 'react-native';
import { ThemeContext } from '../context/themeContext';
import { Paragraph } from 'react-native-paper';
import { globalStyles } from '../globalStyles';

const TrackDetailScreen = ({ navigation }) =>{

    let theme = useContext(ThemeContext);

    return (
        <View style={[globalStyles.container, theme.container]}>
            <ScrollView>
                <View  style={globalStyles.wrapper}>
                    <Text style={{ fontSize: 20, marginBottom: 10 }}>{navigation.getParam('location')}</Text>

                    <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra accumsan in nisl nisi scelerisque. Fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec. Tincidunt id aliquet risus feugiat in ante. Ac felis donec et odio. Bibendum neque egestas congue quisque egestas diam in arcu cursus. Sodales neque sodales ut etiam sit amet nisl purus. Sit amet nulla facilisi morbi tempus iaculis. Turpis egestas maecenas pharetra convallis posuere morbi leo. In iaculis nunc sed augue lacus viverra vitae. Nunc mattis enim ut tellus elementum sagittis vitae et. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium. Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Enim ut sem viverra aliquet eget. Ornare arcu odio ut sem nulla pharetra diam. Odio tempor orci dapibus ultrices in. Auctor neque vitae tempus quam. Habitant morbi tristique senectus et netus et. Etiam dignissim diam quis enim lobortis scelerisque fermentum. {"\n"}</Paragraph>

                    <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Viverra accumsan in nisl nisi scelerisque. Fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate sapien nec. Tincidunt id aliquet risus feugiat in ante. Ac felis donec et odio. Bibendum neque egestas congue quisque egestas diam in arcu cursus. Sodales neque sodales ut etiam sit amet nisl purus. Sit amet nulla facilisi morbi tempus iaculis. Turpis egestas maecenas pharetra convallis posuere morbi leo. In iaculis nunc sed augue lacus viverra vitae. Nunc mattis enim ut tellus elementum sagittis vitae et. Sed egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium. Egestas quis ipsum suspendisse ultrices gravida dictum fusce ut. Enim ut sem viverra aliquet eget. Ornare arcu odio ut sem nulla pharetra diam. Odio tempor orci dapibus ultrices in. Auctor neque vitae tempus quam. Habitant morbi tristique senectus et netus et. Etiam dignissim diam quis enim lobortis scelerisque fermentum.</Paragraph>
                </View>
            </ScrollView>
        </View>
    ) 
};

const styles = StyleSheet.create({
    
})


export default TrackDetailScreen;

