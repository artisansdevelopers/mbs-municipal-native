import React,{ useContext } from 'react';
import { View, Text, TouchableHighlight, TouchableWithoutFeedback, Keyboard, KeyboardAvoidingView } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { NavigationEvents } from 'react-navigation'
import NavLink from '../components/NavLink';
import { Context as AuthContext } from '../context/authContext';
import { ThemeContext } from '../context/themeContext';
import { Formik } from 'formik';
import * as validate from 'yup';
import { globalStyles } from '../globalStyles';
import { LinearGradient } from 'expo-linear-gradient';
import Spacer from '../components/Spacer';
import { MaterialCommunityIcons } from 'react-native-vector-icons';

//Form Validation
const validationSchema = validate.object().shape({
    email:  validate.string().required('Email is required.').email('Please enter a valid Email.'),
  });

const ForgotPasswordScreen = ({ navigation }) =>{

    const { state, clearErrorMessage } = useContext(AuthContext);
    let theme = useContext(ThemeContext);

    return (
        <KeyboardAvoidingView
                style={[globalStyles.login_container, theme.login.container]}
                behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View>
                    <Text style={globalStyles.login_title}>Forgot Password</Text>
                    <Formik 
                        initialValues={{ email: '', password: '' }}
                        validationSchema={validationSchema}
                        onSubmit={ (values, actions) =>{
                            navigation.navigate('Loading', { text: 'Sending Link...' });
                            actions.resetForm();
                            console.log(values);
                        }}>

                        {props => (
                            <View>
                                
                                <Spacer>
                                    <TextInput
                                        onChangeText={props.handleChange('email')}
                                        onBlur={props.handleBlur('email')}
                                        value={props.values.email}
                                        placeholder="Email Address"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType="email-address"
                                        style={[globalStyles.login_input, theme.login.input]}
                                        editable={!state.isLoading}/>
                                    {(props.touched.email && props.errors.email) && 
                                        <Text style={globalStyles.error_text}>{ props.errors.email }</Text>
                                    }
                                </Spacer>
                                
                                <Spacer>
                                    <TouchableHighlight onPress={()=>!state.isLoading && props.handleSubmit()} underlayColor={theme.login.container.backgroundColor}>
                                        <LinearGradient colors={[theme.secondary_color, theme.primary_color]} 
                                            start={[0,0]} 
                                            end={[1,1]} 
                                            style={globalStyles.login_button}>
                                            <Text style={[theme.login.button_text, globalStyles.login_button_text]}>SEND RESET LINK</Text>
                                        </LinearGradient>
                                    </TouchableHighlight>
                                </Spacer>
                            </View>
                            )}             
                    </Formik>
                    
                    <View style={globalStyles.login_ask_container}>
                        <MaterialCommunityIcons name="chevron-double-left" size={20}/><NavLink text="Go Back " routeName="SignIn" style={{ fontWeight: 'bold', marginLeft: 5 }}/>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
};

export default ForgotPasswordScreen;