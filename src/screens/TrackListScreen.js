import React, { useState, useContext } from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { ThemeContext } from '../context/themeContext';
import { TouchableRipple } from 'react-native-paper';
import { EvilIcons } from 'react-native-vector-icons';
import { globalStyles } from '../globalStyles';

const TrackListScreen = ({navigation}) =>{

    const [ tracks, setTrack ] = useState([
        {
            id: 1,
            location: 'Tagbilaran City',
        },
        {
            id: 2,
            location: 'Dauis',
        },
        {
            id: 3,
            location: 'Panglao',
        }
    ]);
    let theme = useContext(ThemeContext);

    return (
        <View style={[globalStyles.container, theme.container]}>
           <FlatList 
                style={{ marginTop: 20 }}
                data={tracks}
                keyExtractor = {(item, index) => index.toString()}
                renderItem={({item}) => (

                    <TouchableRipple 
                        onPress={()=> navigation.navigate('TrackDetail', item)} 
                        rippleColor="rgba(0, 0, 0, 0.1)">

                        <View style={styles.list}>
                            <EvilIcons name="location" size={20} style={{ marginRight: 10 }}/><Text>{item.location}</Text>
                        </View>
                    </TouchableRipple>

                )}/>

        </View>
    )
};

const styles = StyleSheet.create({
    list:{
        padding: 20,
        borderBottomWidth: 1,
        borderBottomColor: '#dcdcde',
        flexDirection: 'row',
        alignItems: 'center'
    }
});


export default TrackListScreen;

