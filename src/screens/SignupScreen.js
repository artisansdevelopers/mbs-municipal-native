import React,{ useContext } from 'react';
import { View, Text, TouchableHighlight, TouchableWithoutFeedback, Keyboard,KeyboardAvoidingView } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import NavLink from '../components/NavLink';
import { Context as AuthContext } from '../context/authContext';
import { ThemeContext } from '../context/themeContext';
import { Formik } from 'formik';
import * as validate from 'yup';
import { globalStyles } from '../globalStyles';
import { LinearGradient } from 'expo-linear-gradient';
import Spacer from '../components/Spacer';

//Form Validation
const validationSchema = validate.object().shape({
    name:  validate.string().required('Name is required.').min(3, 'Name is too short!'),
    email: validate.string().required('Email is required.').email('Please enter a valid Email.'),
    password: validate.string().required('Password is required.').min(3, 'Password is too short!'),
    password_confirmation: validate.string().required('Password Confirmation is required.').oneOf([validate.ref('password'), null], 'Passwords do not match.')
  });

const SignUpScreen = ({ navigation }) =>{

    const { state, signUp } = useContext(AuthContext);
    let theme = useContext(ThemeContext);
    
    return (
        <KeyboardAvoidingView
                style={[globalStyles.login_container, theme.login.container]}
                behavior="padding">
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <View>
                    <Text style={globalStyles.login_title}>Sign Up</Text>
                    <Formik 
                        initialValues={{ name: '',email: '', password: '', password_confirmation: '' }}
                        validationSchema={validationSchema}
                        onSubmit={ (values, actions) =>{
                            navigation.navigate('Loading', { text: 'Creating New Account...' });
                            actions.resetForm();
                            signUp(values);
                        }}>

                        {props => (
                            <View>
                                <Spacer>
                                    <TextInput
                                        onChangeText={props.handleChange('name')}
                                        onBlur={props.handleBlur('name')}
                                        value={props.values.name}
                                        placeholder="Full Name"
                                        autoCorrect={false}
                                        style={[globalStyles.login_input,theme.login.input]}
                                        editable={!state.isLoading}/>
                                    {(props.touched.name && props.errors.name) &&
                                        <Text style={globalStyles.error_text}>{ props.errors.name }</Text>
                                    }
                                </Spacer>
                                
                                <Spacer>
                                    <TextInput
                                        onChangeText={props.handleChange('email')}
                                        onBlur={props.handleBlur('email')}
                                        value={props.values.email}
                                        placeholder="Email Address"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        keyboardType="email-address"
                                        style={[globalStyles.login_input,theme.login.input]}
                                        editable={!state.isLoading}/>
                                    {(props.touched.email && props.errors.email) &&
                                        <Text style={globalStyles.error_text}>{ props.errors.email }</Text>
                                    }
                                </Spacer>
                                
                                <Spacer>
                                    <TextInput
                                        onChangeText={props.handleChange('password')}
                                        onBlur={props.handleBlur('password')}
                                        value={props.values.password}
                                        placeholder="Password"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        secureTextEntry
                                        style={[globalStyles.login_input,theme.login.input]}
                                        editable={!state.isLoading}/>
                                    {(props.touched.password && props.errors.password) &&
                                        <Text  style={globalStyles.error_text}>{ props.errors.password }</Text>
                                    }
                                </Spacer>

                                <Spacer>
                                    <TextInput
                                        onChangeText={props.handleChange('password_confirmation')}
                                        onBlur={props.handleBlur('password_confirmation')}
                                        value={props.values.password_confirmation}
                                        placeholder="Confirm Password"
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                        secureTextEntry
                                        style={[globalStyles.login_input,theme.login.input]}
                                        editable={!state.isLoading}/>
                                    {(props.touched.password_confirmation && props.errors.password_confirmation) &&
                                        <Text  style={globalStyles.error_text}>{ props.errors.password_confirmation }</Text>
                                    }
                                </Spacer>
                                
                                <Spacer>
                                    <TouchableHighlight onPress={()=>!state.isLoading && props.handleSubmit()} underlayColor={theme.login.container.backgroundColor}>
                                        <LinearGradient colors={[theme.secondary_color, theme.primary_color]} 
                                            start={[0,0]} 
                                            end={[1,1]} 
                                            style={globalStyles.login_button}>
                                            <Text style={[theme.login.button_text, globalStyles.login_button_text]}>CREATE NEW ACCOUNT</Text>
                                        </LinearGradient>
                                    </TouchableHighlight>
                                </Spacer>
                            </View>
                            )}             
                    </Formik>
                    
                    <View style={globalStyles.login_ask_container}>
                        <Text>Already have account? </Text><NavLink text="Sign In " routeName="SignIn" style={{ fontWeight: 'bold' }}/>
                    </View>
                </View>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
    )
};

export default SignUpScreen;

