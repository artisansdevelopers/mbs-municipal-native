import React,{useContext, useEffect} from 'react';
import {Context as AuthContext} from '../context/authContext';

const AuthResolve = () =>{
    const { tryLocalSignin } = useContext(AuthContext);
    useEffect(()=>{
        tryLocalSignin();
    },[]) //this parameter means to run only once
    return null;
};


export default AuthResolve;

