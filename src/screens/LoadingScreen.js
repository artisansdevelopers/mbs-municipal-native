import React, { useEffect, useContext } from 'react';
import { View, StyleSheet, Text, ActivityIndicator, BackHandler } from 'react-native';
import { ThemeContext } from '../context/themeContext';

const LoadingScreen = ({ navigation }) =>{

    useEffect(()=>{
        function preventBack(){
            navigation.push('Loading');
        }
        BackHandler.addEventListener('hardwareBackPress', preventBack);
        return () =>{
            BackHandler.removeEventListener('hardwareBackPress', preventBack);
        }
    }, []);

    let theme = useContext(ThemeContext);

    return (
        <View style={[styles.loading_wrapper, theme.loading.background]}>
            <View>
                <ActivityIndicator size="large" color={theme.primary_color} />
                <Text style={styles.loading_text}>{navigation.getParam('text')}</Text>
            </View>
        </View>
    )
};

LoadingScreen.navigationOptions = () => {
    return {
        headerShown: false
    }
}

const styles = StyleSheet.create({
    loading_wrapper:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    loading_text:{
        color: '#858585',
        marginTop: 15
    }
})


export default LoadingScreen;
