import {AsyncStorage, Alert} from 'react-native';
import createDataContext from './createDataContext';
import laravelAPI from '../api/laravel';
import {navigate} from '../navigationRef';

const authReducer = (state, action) => {
    switch(action.type){
        case 'ADD_ERROR':
            return {...state, errorMessage: action.payload};
        case 'SIGN_IN':
            return { errorMessage: null, token: action.payload};
        case 'CLEAR_ERROR_MESSAGE':
            return {...state, errorMessage: null};
        case 'SIGN_OUT':
            return {token : null, errorMessage: null};
        case 'SET_LOADING':
            return {...state, isLoading: action.payload}
        default : 
        return state;
    }

};

const tryLocalSignin = dispatch => async () => {
    const token = await AsyncStorage.getItem('token');
    if(token){
        dispatch({ //dispatch updates the state, just liek setState
            type : 'SIGN_IN',
            payload: token
        })
        navigate('TrackList');
    }else{
    navigate('LoginFlow')
    }
    
}

const clearErrorMessage = dispatch => () => {
    dispatch({ //dispatch updates the state, just liek setState
        type : 'CLEAR_ERROR_MESSAGE'
    })
}
    


const signUp = (dispatch) => async ({email, password,name}, callback) =>{
    dispatch({type: 'SET_LOADING', payload: true});
    try{  
        const response = await laravelAPI.post('api/v1/auth/register',{email, password,name});
        if(response.data.status === 200){
            navigate('SignIn');
        }   
        dispatch({type: 'SET_LOADING', payload: false});      
    }catch(err){
        // dispatch({
        //     type: 'ADD_ERROR',
        //     payload: ' Credentials are already existed. Sign In instead?'
        // })
        Alert.alert('Oops!','Credentials are already existed. Sign In instead?');
        dispatch({type: 'SET_LOADING', payload: false});
        navigate('SignUp');
    }
}


const signIn = (dispatch) => async ({email, password}, callback) =>{
    dispatch({type: 'SET_LOADING', payload: true});
    try{  
        const response = await laravelAPI.post('api/v1/auth/login',{email, password});
        await AsyncStorage.setItem('token', response.data.access_token);
        dispatch({type: 'SIGN_IN', payload: response.data.access_token});
        dispatch({type: 'SET_LOADING', payload: false});
        navigate('MainFlow');
    }catch(err){
            // dispatch({
            //     type: 'ADD_ERROR',
            //     payload: ' Credentials were incorrect! Please try again.'
            // })
        Alert.alert('Oops!','Credentials were incorrect! Please try again.');
        dispatch({type: 'SET_LOADING', payload: false});
        navigate('SignIn');
    }
}


const signOut = (dispatch) => async () =>{
    await AsyncStorage.removeItem('token');
    dispatch({type: 'SIGN_OUT'});
    setTimeout(()=>{
        navigate('LoginFlow');
    }, 3000)
}

export const { Provider, Context } = createDataContext(
    authReducer,
    { 
        signIn, 
        signOut, 
        signUp, 
        clearErrorMessage, 
        tryLocalSignin
    },
    {
        token: null, 
        errorMessage: null, 
        success: null, 
        isLoading: false
    }
)
