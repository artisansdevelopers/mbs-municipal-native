import React from 'react';

export const themes = {
  default: {
    default_text: '#000',
    primary_color: '#89216B',
    secondary_color: '#DA4453',
    login:{ //Login Page Colors
      container: {
        backgroundColor: '#f2f2f2'
      },
      input: {
        backgroundColor: '#fff',
        borderColor: '#dcdcde'
      },
      button_text: {
        color: '#fff'
      }
    },
    container:{// Page Container Colors
      backgroundColor: '#f7f7f7',
    },
    header: { //Header Colors
      color: {
        color: '#fff'
      },
      border: {
        borderBottomColor: '#dcdcde'
      }
    },
    bottom_tabs:{ //Bottom Tab Colors
      background: '#fff',
      border: '#dcdcde'
    },
    loading: {// Loading Page Colors
      background: {
        backgroundColor: '#fff'
      }
    }
  },
};

export const ThemeContext = React.createContext(
  themes.default // default theme
);