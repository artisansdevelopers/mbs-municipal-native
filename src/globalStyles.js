import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
    container: {
        flex: 1
    },
    wrapper: {
        flex: 1,
        padding: 20,
    },
    header:{
        height: 60,
        alignItems: 'flex-start',
        justifyContent: 'center',
        elevation: 0,
        borderBottomWidth: 1,
    },
    header_button: {
        position: 'absolute',
        left: 10,
        width: 35,
        height: 35,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        zIndex: 1
    },
    header_text: {
        fontSize: 14,
        paddingLeft: 50,
    },
    login_input:{
        borderWidth: 1,
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderRadius: 25,
        fontSize: 15,
    },
    error_text:{
        paddingHorizontal: 15,
        color: 'red',
        fontSize: 12,
        marginTop: 5
    },
    login_container:{
        flex: 1,
        padding: 20,
        justifyContent: 'center',
    },
    login_title:{
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        marginBottom: 15
    },
    login_ask_container:{
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        marginTop: 15
    },
    login_button:{
        padding: 10,
        height: 40,
        borderRadius: 25,
        marginTop: 15
    },
    login_button_text:{
        textAlign: 'center'
    }
});
  