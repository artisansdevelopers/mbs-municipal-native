import React from 'react';
import { View } from 'react-native';
import {Provider as AuthProvider} from './src/context/authContext';
import {Provider as LocationProvider} from './src/context/LocationContext';
import {setNavigator} from './src/navigationRef';
import Navigator from './src/routes/Navigator';
import StandardStatusBar from './src/components/StandardStatusBar';

export default function App(){
  return(
  <LocationProvider>
    <AuthProvider>
      <View style={{flex: 1}}>
        <StandardStatusBar backgroundColor="black" barStyle="light-content"/>
        <Navigator 
          ref={navigator => {
            setNavigator(navigator);
          }} 
        />
        </View>
    </AuthProvider>
  </LocationProvider>
  );
};